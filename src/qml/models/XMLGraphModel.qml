import QtQuick 2.2
import QtQuick.XmlListModel 2.0

XmlListModel {
  source: graphGXmlPath

  namespaceDeclarations: "declare default element namespace 'ips://galacteek.ld/';"

  Component.onCompleted: {
    console.log('Using source ' + source)
  }

  /* XML roles */

  XmlRole {
    name: "did"
    query: "did/id/string()"
  }

  XmlRole {
    name: "personNickName"
    query: ".//*[self::nickName]/string()"
  }
}
