SparQLModel {
  query: "
    SELECT ?url ?rscType
    WHERE {                                                                       
      ?rsc a ?type ;
        gs:sharedContent/a ?rscType ;
        gs:sharedContent/gs:url ?url .
    }"
}
