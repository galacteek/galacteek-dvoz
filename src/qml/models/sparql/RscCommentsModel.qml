SparQLModel {
  /*
   * Model for comments about a specific resource
   * (can be an article, social post, any creative thing)
   *
   */
  query: "
    SELECT ?commentUri ?body
    WHERE {                                                                       
      ?commentUri a gs:Comment ;
        gs:text ?body ;
        gs:about ?rsc .
    }"
}
