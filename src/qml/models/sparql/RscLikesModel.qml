SparQLModel {
  query: "
    SELECT ?uri ?agent
    WHERE {                                                                       
      ?uri a gs:LikeAction ;
        gs:agent ?agent ;
        gs:object ?rsc .
    }"
}
