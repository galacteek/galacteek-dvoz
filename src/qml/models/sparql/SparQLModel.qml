import QtQuick 2.2

ListModel {
  /*
   * ListModel for SparQL results.
   * Queries are executed against galacteek's RDF graphs
   *
   * The model roles match the SparQL variables names
   *
   * Defines custom prefixes via the 'prefixes' property
   * Define your SparQL query with the 'query' variant property
   * Define the query 'bindings' via the 'bindings' variant
   *
   */

  dynamicRoles: true

  property string ident: null // Model identifier
  property bool debug: false
  property string query: ""
  property string graphIri: "urn:ipg:g:c0"
  property variant bindings: null
  property variant prefixes: null
  property int limit: -1
  property variant stdPrefixes: {'gs': 'ips://galacteek.ld/'}

  signal sparkle

  function run() {
    clear()

    var fquery = ''

    for (var ns in stdPrefixes) {
       fquery += `PREFIX ${ns}: <` + stdPrefixes[ns] + '>'
    }

    fquery += query

    if (limit > 0) {
      fquery += `\nLIMIT ${limit}`
    }

    if (debug) {
      console.debug('Running query ' + fquery)
    }

    var results = sparql.query(fquery, graphIri, bindings)

    if (debug) {
      console.debug('Got results ' + JSON.stringify(results))
    }

    results.forEach(function(row) {
      if (debug) {
        console.debug('Got result')
      }
      append(row)
    })

    sparkle()
  }

  Component.onCompleted: {
    run()
  }

  function resultsSearch(varname, value) {
    /* Search for a value in the model */
    for (let idx=0; idx < count; idx++) {
      let item = get(idx)

      if (item[varname] === value) {
        return item
      }
    }
  }
}
