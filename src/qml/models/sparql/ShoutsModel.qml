SparQLModel {
  query: "
    SELECT ?uri ?body ?headline ?dateCreated ?language
    WHERE {
      ?uri a gs:DwebSocialPost ;
        gs:headline ?headline ;
        gs:articleBody ?body ;
        gs:dateCreated ?dateCreated ;
        gs:inLanguage/gs:alternateName ?language .
    }
    ORDER BY DESC(?dateCreated)
  "
}
