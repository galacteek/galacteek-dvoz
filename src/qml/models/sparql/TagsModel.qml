SparQLModel {
  query: "
    SELECT ?uri ?tag
    WHERE {
      ?uri a gs:DefinedTerm ;
        gs:termCode ?tag .
    }
  "
  debug: true
}
