SparQLModel {
  query: "
    SELECT DISTINCT ?follower
    WHERE {                                                                       
      ?follower a gs:Person ;
        gs:follows ?followed .
    }"
}
