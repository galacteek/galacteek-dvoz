SparQLModel {
  query: "
    SELECT DISTINCT ?followed ?followedNick
    WHERE {                                                                       
      ?follower a gs:Person ;
        gs:follows/gs:nickName ?followedNick ;
        gs:follows ?followed .
    }"
}
