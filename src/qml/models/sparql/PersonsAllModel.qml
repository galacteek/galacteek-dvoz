SparQLModel {
  query: "
    SELECT DISTINCT ?person ?personNick
    WHERE {                                                                       
      ?person a gs:Person ;
        gs:nickName ?personNick .

      FILTER(?personNick != '')
    }"
}
