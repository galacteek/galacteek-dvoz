import QtQuick 2.4

ListModel {
  id: peopleModel

  ListElement {
    title: qsTr("All")
    page: "pages/people/All.qml"
  }

  ListElement {
    title: qsTr("Following")
    page: "pages/people/Following.qml"
  }
}
