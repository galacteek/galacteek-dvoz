import QtQuick 2.2

JSONListModel {
  id: languagesModel
  source: "languages.json"
  query: "$.*"

  function indexForLanguage(name) {
    // Return the model's index for a given language, or -1 if not found
    var model = languagesModel.model

    for (let idx=0; idx < model.count; idx++) {
      let item = model.get(idx)
      if (item.name === name) {
        return idx
      }
    }
    return -1
  }
}
