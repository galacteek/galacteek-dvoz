import QtQuick 2.4

ListModel {
  id: pageModel

  ListElement {
    title: qsTr("Identity")
    page: "pages/ipid/IdentityManage.qml"
  }

  ListElement {
    title: qsTr("Crowd")
    page: "pages/people/Menu.qml"
  }

  ListElement {
    title: qsTr("Write an article")
    page: "pages/ArticleWritePage.qml"
  }

  ListElement {
    title: qsTr("Recent articles")
    page: "pages/ArticlesListing.qml"
  }

  ListElement {
    title: qsTr("Shout")
    page: "pages/shout/ShoutsView.qml"
  }

  ListElement {
    title: qsTr("About")
    page: "pages/About.qml"
  }

  /*
  ListElement {
    title: qsTr("SparQL tester")
    page: "pages/tools/SparQLTester.qml"
  }
  ListElement {
    title: qsTr("Video channels")
    page: "pages/MultimediaChannels.qml"
  }
  ListElement {
    title: qsTr("Share a video")
    page: "pages/VideoShare.qml"
  }
  */
}
