import QtQuick 2.2
import QtQuick.Controls 1.2

import "../../delegates"
import "../../models"
import "../../models/sparql"
import "../../views"
import ".."

Page {
  PersonsAllModel {
    id: model
  }

  SleekMenuView {
    id: view
    model: model
    delegate: MenuDelegate {
      text: personNick

      onClicked: {
        stack.iOpen(person)
      }
    }
  }
}
