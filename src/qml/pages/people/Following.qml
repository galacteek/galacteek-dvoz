import QtQuick 2.2
import QtQuick.Controls 1.2

import "../../delegates"
import "../../models"
import "../../models/sparql"
import "../../views"
import ".."

Page {
  FollowingModel {
    id: fModel
    bindings: {'follower': passport.me['@id']}
    debug: true
  }

  SleekListView {
    id: view
    model: fModel
    anchors.fill: parent
    delegate: MenuDelegate {
      text: followedNick

      onClicked: {
        stack.iOpen(followed)
      }
    }
  }
}
