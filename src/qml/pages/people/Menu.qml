import QtQuick 2.2
import QtQuick.Controls 1.2

import ".."
import "../../models"
import "../../views"

Page {
  SleekMenuView {
    id: view
    model: PeopleMenuModel { }
    anchors.fill: parent
  }
}
