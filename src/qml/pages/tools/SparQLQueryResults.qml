import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.3

import "../../FormInputs"
import "../../models"
import "../../models/sparql"
import ".."

Page {
  ColumnLayout {
    anchors.horizontalCenter: parent.horizontalCenter
    Row {
      IText {
        text: qsTr("SparQL query")
      }
    }

    Row {
      ITextArea {
        id: query
        width: parent.wid
      }
    }
    Row {
      IButton {
        text: "Run query"
        onClicked: {
          console.debug(stack)
          stack.pushPage('pages/tools/SparQLQueryResults.qml')
        }
      }
    }
  }
}
