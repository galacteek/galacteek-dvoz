import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.3

import "../../FormInputs"
import "../../models"
import "../../models/sparql"
import ".."

Page {
  SparQLModel {
    id: smodel
    debug: true
  }

  ColumnLayout {
    // anchors.fill: parent
    // anchors.horizontalCenter: parent.horizontalCenter

    Row {
      IText {
        text: qsTr("SparQL query")
      }
    }

    Row {
      width: parent.width
      ITextArea {
        width: parent.width
        id: query
        font.pixelSize: 24
        text: "
SELECT ?uri
WHERE {
  ?uri a gs:DwebSocialPost .
}
        "
      }
    }
    Row {
      id: rowRun
      IButton {
        text: "Run query"
        onClicked: {
          smodel.query = query.text
          console.debug(smodel.query)
          smodel.run()
        }
      }
    }

    ListView {
      anchors.top: rowRun.bottom
      model: smodel
      delegate: Rectangle {
        color: "white"
        Text {
          text: uri
        }
      }
    }
  }
}
