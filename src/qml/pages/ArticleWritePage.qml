import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import "../pages"
import "../FormInputs" as Form

Page {
  id: view
  width: parent.width
  height: parent.height

  ColumnLayout {
    id: form
    spacing: 30
    anchors.centerIn: parent
    anchors.horizontalCenter: parent.horizontalCenter

    Form.Headline {
      id: headline
    }

    Form.HeadlineAlt {
      id: altHeadline
    }

    Form.Language {
      id: language
    }

    Loader {
      id: body
    }

    Form.IButton {
      id: publish
      text: qsTr("Next")

      onClicked: {
        if (headline.length === 0) {
          headline.field.textColor = "red"
          return
        }

        stack.push({
          item: Qt.resolvedUrl('./ArticleWritePageBody.qml'),
          properties: {
            stack: stack,
            headline: headline.text,
            altHeadline: altHeadline.text,
            language: language.ld()
          }
        })
      }
    }
  }
}
