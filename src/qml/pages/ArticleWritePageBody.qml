import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import "../FormInputs" as Form
import "../pages"

Page {
  id: view

  property string altHeadline
  property string headline
  property variant language

  Form.IText {
    anchors.horizontalCenter: parent.horizontalCenter
    text: headline
    font.pixelSize: 18
  }

  Form.ITextArea {
    anchors.centerIn: parent
    anchors.horizontalCenter: parent.horizontalCenter
    id: body
    font.pixelSize: 24
    width: parent.width - 100
    height: parent.height - 120
  }

  ColumnLayout {
    id: form
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: body.bottom

    Form.IButton {
      id: publish
      text: qsTr("Publish")

      onClicked: {
        let id = ld.iObjectUriGen("Article")

        let result = ld.iObjectCreate(id, {
          "@type": "Article",
          "@id": id,
          "articleMarkdownBody": body.text,
          "headline": headline,

          "alternativeHeadline": altHeadline,
          "inLanguage": language
        })

        stack.goHome()
      }
    }
  }
}
