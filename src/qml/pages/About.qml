import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.3
import "../FormInputs"

Page {
  ColumnLayout {
    anchors.fill: parent

    ITextArea {
      anchors.fill: parent
      readOnly: true
      textFormat: Text.RichText
      font.pixelSize: 24
      textMargin: 32

      text: qsTr("
      <h1>Dvoz</h1>
      <p>
      Dvoz (for <i>Decentralized voice</i>), is a decentralized
      platform for unconstrained P2P social exchanges. It uses
      the <i>pronto</i> ontological chain system, building upon
      distributed <i>RDF</i> graphs.
      </p>

      <p>It is fully opensource and is written with an hybrid
      QML/web stack. It is the main <i>dapp</i> for <i>galacteek</i>,
      which is an ontologic agent for the distributed web.
      </p>

      <p>
      It uses <i>Linked data</i> for everything,
      with the stack including the following technologies:

      <ul>
      <li>IPFS (the <i>InterPlanetary FileSystem</i>),
        for storage of content and schemas</li>
      <li>JSON-LD and JSON-LD signatures</li>
      <li>RDF and SparQL</li>
      </ul>
      </p>
      ")
    }
  }
}
