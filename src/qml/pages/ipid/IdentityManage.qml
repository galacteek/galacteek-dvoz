import QtQuick 2.2
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import ".."
import "../../FormInputs"
import "../../ipfs"

Page {
  id: identityPage

  width: parent.width
  height: parent.height
  anchors.top: parent.top

  property var compacted
  // property var passport

  Component.onCompleted: {
    didCurrent = ipid.did()
    // compacted = ipid.compacted()
    // passport = ipid.serviceEndpointCompact('/passport')
  }

  ColumnLayout {
    anchors.horizontalCenter: parent.horizontalCenter
    spacing: 32

    Nickname {
      id: nickname
      text: passport.me.nickName
    }

    Row {
      IText {
        text: qsTr('First name')
      }
      ITextField {
        id: firstName
        text: passport.me.givenName
        maximumLength: 16
      }
    }

    Row {
      IText {
        text: qsTr('Last name')
      }
      ITextField {
        id: lastName
        text: passport.me.name
        maximumLength: 32
      }
    }

    Language {
      id: mainLanguage
      preLang: passport.mainLanguage.name
      label: qsTr("Main language")
    }

    Language {
      id: secondaryLanguage
      preLang: passport.secondLanguage.name
      label: qsTr("Secondary language")
    }

    IButton {
      text: qsTr("Update")
      onClicked: {
        ipid.serviceEndpointPatch(
          '/passport', {
            'me': {
              '@type': 'Person',
              'nickName': nickname.text,
              'givenName': firstName.text,
              'name': lastName.text
            },
            'mainLanguage': mainLanguage.ld(),
            'secondLanguage': secondaryLanguage.ld()
          }
        )

        identityPage.parent.pop()
      }
    }
  }
}
