import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.3
import "../ipfs"
import "../FormInputs"
import "../delegates"
import "../views"

Page {
  id: articles

  width: parent.width
  height: parent.height
  anchors.top: parent.top

  function articlesSearch(lang) {
    let code = lang.alternateName

    if (code) {
      console.debug(code)
    } else {
      code = "en"
    }

    modelArticles.clearModel()

    modelArticles.graphQuery("
      PREFIX Article: <ips://galacteek.ld/Article>

      SELECT ?uri ?headline ?dateCreated ?language
      WHERE {
          ?uri a gs:Article ;
            gs:headline ?headline ;
            gs:dateCreated ?dateCreated ;
            gs:inLanguage/gs:alternateName ?language .
          FILTER(str(?language) = \"%1\")
      }
      ORDER BY DESC(?dateCreated)
      LIMIT 100
    ".arg(code))
  }

  Component.onCompleted: {
    articlesSearch({})
  }

  Component {
    id: articlesHeader

    RowLayout {
      spacing: 42

      Language {
        id: sLang
      }

      IButton {
        id: search
        text: "Search"
        onClicked: {
          articlesSearch(sLang.ld())
        }
      }
    }
  }

  SleekListView {
    id: view
    anchors.fill: parent
    anchors.margins: 8
    model: modelArticles
    header: articlesHeader

    delegate: MenuDelegate {
      text: headline

      onItemEntered: {
        view.currentIndex = index
      }

      onClicked: {
        stack.iOpen(uri)
      }
    }
  }
}
