import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.3

import "../../ipfs"
import "../../views"
import "../../FormInputs"
import "../../delegates"
import "../../models"
import "../../models/sparql"
import ".."

Page {
  id: shoutsView

  function shoutsSearch(lang) {
    let code = lang.alternateName

    if (code) {
      console.debug(code)
    } else {
      code = "en"
    }

    /*
    modelShouts.prepare(
      "shoutComments",
      "PREFIX gs: <ips://galacteek.ld/>

      SELECT ?uri ?body
      WHERE {                                                                       
        ?uri a gs:Comment ;
          gs:text ?body ;
          gs:about ?rsc .
      }"
    )
    */

    modelShouts.runPreparedQuery("shoutsSearch", {
      langSearch: code,
      tag: tagSelector.tag
    })
  }

  Component.onCompleted: {
    modelShouts.prepare("shoutsSearch", "
      PREFIX gs: <ips://galacteek.ld/>
      PREFIX DwebSocialPost: <ips://galacteek.ld/DwebSocialPost>

      SELECT ?uri ?body ?headline ?dateCreated ?language ?authorNickName
      WHERE {
          ?uri a gs:DwebSocialPost ;
            gs:headline ?headline ;
            gs:articleBody ?body ;
            gs:dateCreated ?dateCreated ;
            gs:author/gs:nickName ?authorNickName ;
            gs:inLanguage/gs:alternateName ?language .
      }
      ORDER BY DESC(?dateCreated)
    ")
    // gs:keywords/gs:termCode ?tag .

    shoutsSearch({})
  }

  ColumnLayout {
    id: actions

    RowLayout {
      IButton {
        id: shout
        text: qsTr("Post")
        onClicked: {
          stack.pushPage("pages/shout/Shout.qml")
        }
      }
      IButton {
        id: search
        text: "Update"
        onClicked: {
          shoutsSearch({})
        }
      }
    }

    RowLayout {
      property StackView stack: null
      visible: false

      Language {
        id: sLang
      }

      TagSelector {
        id: tagSelector
        visible: false
      }

    }
  }

  Item {
    anchors.top:  actions.bottom
    width: parent.width
    height: parent.height

    SleekListView {
      id: sView
      anchors.fill: parent

      model: modelShouts

      delegate: MenuDelegate {
        property RscCommentsModel cmodel: RscCommentsModel {
          id: cmodel
          bindings: { 'rsc': uri }
        }
        property RscLikesModel likesModel: RscLikesModel {
          id: likesModel
          bindings: { 'rsc': uri }
        }

        id: shoutItem
        text: headline

        onItemEntered: {
          sView.currentIndex = index
        }

        onClicked: {
          stack.iOpen(uri)
        }
      }
    }
  }
}
