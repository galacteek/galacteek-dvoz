import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.3

import "../../FormInputs"
import "../../delegates"
import "../../models"
import "../../models/sparql"
import ".."

Page {
  id: shoutView

  property string uri: ""
  property var passport

  ShoutModel {
    id: shoutModel
    bindings: { 'uri': uri }

    onSparkle: {
      var shout = shoutModel.get(0)
    }
  }

  Loader {
    id: shoutLoader
    anchors.fill: parent
  }
}
