import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.1
import QtQuick.Layouts 1.3
import "../../FormInputs" as Form
import "../../ipfs"
import ".."

Page {
  id: shoutNew

  ListModel {
    id: attachmentsModel
  }

  RowLayout {
    id: mfsLayout
    visible: false
    width: parent.width * 0.8
    height: parent.height * 0.8

    MfsView {
      id: filesView

      onIpfsObjectSelected: {
        formLayout.visible = true
        mfsLayout.visible = false

        attachmentsModel.append({
          'url': url,
          'mimeType': mimeType
        })
      }
    }
  }

  ColumnLayout {
    id: formLayout
    spacing: 32
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: parent.top

    Form.Message {
      id: message
    }

    Form.Headline {
      id: headline
    }

    Form.Language {
      id: language
    }

    Form.ArticleBody {
      id: body
      textAreaWidth: shoutNew.width * 0.6
    }

    Row {
      ListView {
        visible: false
        model: attachmentsModel
        delegate: Rectangle {
          Form.IText {
            text: url
          }
        }
      }
    }

    Row {
      spacing: 16
      Form.IButton {
        text: qsTr("Attach a file (IPFS MFS)")
        onClicked: {
          formLayout.visible = false
          mfsLayout.visible = true
        }
      }

      Form.IButton {
        id: publish
        text: qsTr("Post")

        onClicked: {
          // myString.match(/#[a-z]+/gi);
          // match(/#[\p{L}]+/ugi)
          // yourText.split(' ').filter(v=> v.startsWith('#'))
          // .split(/[\s\n\r]/gmi).filter(v=> v.startsWith('#'))
          // .match(/#[^\s#]*/gmi);

          if (headline.length === 0) {
            message.messageText = qsTr("Empty headline ...")
            message.open()
            return
          }

          if (body.text.length === 0) {
            message.messageText = qsTr("Empty post ...")
            message.open()
            return
          }

          let tagsList = body.text.match(/#[^\s#]*/gmi)
          let tags = []

          if (tagsList) {
            tagsList.forEach(function(tag) {
              tags.push({
                "@type": "DefinedTerm",
                "termCode": tag
              })
            })
            console.debug('TAGS ' + JSON.stringify(tags))
          }

          let id = ld.iObjectUriGen("DwebSocialPost")

          let obj = {
            "@type": "DwebSocialPost",
            "@id": id,
            "articleBody": body.text,
            "headline": headline.text,
            "inLanguage": language.ld(),
            "keywords": tags,
            "sharedContent": []
          }

          for (var i=0; i < attachmentsModel.count; i++) {
            var url = attachmentsModel.get(i).url
            var mime = attachmentsModel.get(i).mimeType
            let type = 'Thing'

            console.debug(url + ': mime is ' +  mime)

            // TODO
            if (mime === 'image/png') {
              type = 'Image'
            } else {
              type = 'Thing'
            }

            obj.sharedContent.push({
              "@type": type,
              "url": url
            })
          }

          let result = ld.iObjectCreate(id, obj)

          stack.pop()
        }
      }
    }
  }
}
