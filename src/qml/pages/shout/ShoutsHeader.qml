import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.3
import "../../FormInputs"

RowLayout {
  property StackView stack: null

  Language {
    id: sLang
  }

  TagSelector {
    id: tagSelector
  }

  IButton {
    id: search
    text: "Search"
    onClicked: {
      console.debug(stack)
    }
  }

  IButton {
    id: shout
    text: qsTr("Shout")
    onClicked: {
      stack.pushPage("pages/shout/Shout.qml")
    }
  }
}
