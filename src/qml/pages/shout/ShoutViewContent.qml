import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import "../../FormInputs"
import "../../delegates"
import "../../models"
import "../../models/sparql"
import ".."

Page {
  id: shoutViewContent

  property var shout

  property RscAttachmentsModel amodel: RscAttachmentsModel {
    id: attsModel
    bindings: { 'rsc': uri }
    onSparkle: {
      console.debug('Attachments count ' + count)
    }
  }
  property RscCommentsModel cmodel: RscCommentsModel {
    id: commentsModel
    bindings: { 'rsc': uri }
  }
  property RscLikesModel likesModel: RscLikesModel {
    id: likesModel
    bindings: { 'rsc': uri }

    onSparkle: {
      let match = likesModel.resultsSearch(
        'agent', passport.me['@id'])
    }
  }

  Component.onCompleted: {
    console.debug('View shout ' + shout.headline)
    passport = ipid.serviceEndpointCompact('/passport')
  }


    ColumnLayout {
      anchors.fill: parent
      id: column
      RowLayout {
        IText {
          text: shout.headline
          font.pixelSize: 42
          color: "blue"
        }
        IText {
          text: Qt.formatDateTime(shout.dateCreated)
        }
      }

      RowLayout {
        Layout.fillHeight: true
        Layout.fillWidth: true
        ITextArea {
          Layout.fillWidth: true
          Layout.fillHeight: true
          id: bodyItem
          textFormat: Text.RichText
          textMargin: 32
          font.pixelSize: 32
          readOnly: true
          text: g.mdToHtml(shout.body)
        }

        IText {
          text: likesModel.count + ' likes'
          visible: false
        }
      }

      RowLayout {
        Layout.fillHeight: true
        ListView {
          Layout.fillHeight: true
          model: attsModel
          // anchors.fill: parent
          orientation: Qt.Vertical

          delegate: Rectangle {
            id: attachItem
            Layout.fillHeight: true
            anchors.fill: parent

            states: [
              State {
                name: "matrix"
                PropertyChanges {
                    target: loader
                    sourceComponent: attachItem
                    Layout.fillWidth: true;  // the layout is set here
                    Layout.fillHeight: true;  // the layout is set here
                }
              }
            ]

              Loader {
                anchors.fill: parent
                sourceComponent: attachItem
                id: loader
              }

            Component.onCompleted: {
              console.debug('Showing att ' + rscType + ' is url ' + url)

              if (rscType === 'ips://galacteek.ld/Image') {
                loader.setSource(
                  '../../ipfs/DwebImage.qml',
                  { "url": url, "stack": stack }
                )
              }
              if (rscType === 'ips://galacteek.ld/VideoObject') {
                loader.setSource(
                  '../../ipfs/IpfsVideoPlayer.qml',
                  { "videoUrl": url, "stack": stack }
                )
              }
            }
        }
      }
    }
  }
}
