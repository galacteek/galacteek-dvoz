import QtQuick 2.2
import QtQuick.Controls 1.2
import "../ipfs"

Page {
  property string url

  Component.onCompleted: {
    console.log('Loading ' + url)
  }

  IpfsWebView {
    anchors.fill: parent
    url: parent.url
  }
}
