import QtQuick 2.8
import QtQuick.Controls 1.2
import "delegates"
import "pages"
import "models"
import "views"

Item {
  visible: true
  width: 640
  height: 480

  property HomePageModel homeModel: HomePageModel { id: homeModel }

  property string didCurrent

  Connections {
    target: iContainer
    onSizeChanged: {
      width = iContainer.getWidth() - 16
      height = iContainer.getHeight() - 16
      console.debug('Container size changed')
    }
  }

  Component.onCompleted: {
    console.log('Started dvoz')
  }

  Rectangle {
    color: "#212126"
    anchors.fill: parent
  }

  ToolBar {
    id: toolbarTop
    height: 80

    BorderImage {
      border.bottom: 12
      source: "images/toolbar.png"
      width: parent.width
      height: parent.height

      Rectangle {
        border.width: 0
        id: backButton
        width: opacity ? 60 : 0
        anchors.left: parent.left
        anchors.leftMargin: 30
        opacity: stackView.depth > 1 ? 1 : 0
        anchors.verticalCenter: parent.verticalCenter
        antialiasing: true
        height: 70
        radius: 0
        color: backmouse.pressed ? "#222" : "transparent"
        Behavior on opacity { NumberAnimation{} }
        Image {
          anchors.verticalCenter: parent.verticalCenter
          source: "images/navigation_previous_item.png"
        }
        MouseArea {
          id: backmouse
          anchors.fill: parent
          anchors.margins: -10
          onClicked: stackView.pop()
        }
      }
    }
  }

  StackView {
    id: stackView
    anchors.fill: parent
    anchors.top: parent.top
    anchors.topMargin: toolbarTop.height + 10

    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenterOffset: 30
    anchors.verticalCenter: parent.verticalCenter
    focus: true

    function iOpen(url) {
      stackView.push({
        item: Qt.resolvedUrl("pages/ILoadResource.qml"),
        properties: { url: url, stack: stackView }
      })
    }

    function pushPage(page) {
      stackView.push({
        item: Qt.resolvedUrl(page),
        properties: {
          stack: stackView
        }
      })
    }

    function goHome() {
      stackView.pop(null)
    }
 
    Keys.onUpPressed: {
      homeView.decrementCurrentIndex()
    }

    Keys.onDownPressed: {
      homeView.incrementCurrentIndex()
    }

    Keys.onReturnPressed: {
      homeView.enterCurrent()
    }

    // Implements back key navigation
    // focus: true
    // Keys.onReleased: if (event.key === Qt.Key_Back && stackView.depth > 1) {
    //            stackView.pop();
    //           event.accepted = true;
    //         }
    //
    //

    initialItem: Item {
      width: parent.width
      height: parent.height

      SleekMenuView {
        id: homeView
        model: homeModel
        anchors.fill: parent

        function enterCurrent() {
          stackView.push(Qt.resolvedUrl(currentItem.page))
        }
      }
    }
  }
}
