import QtQuick 2.8
import QtQuick.Controls 1.2
import "../delegates"

ListView {
  id: view
  anchors.fill: parent

  highlight: highlightBar
  highlightFollowsCurrentItem: true

  boundsBehavior: Flickable.StopAtBounds

  delegate: MenuDelegate {
    text: title

    onItemEntered: {
      view.currentIndex = index
    }
    onClicked: {
      stackView.pushPage(page)
    }
  }

  Component {
     id: highlightBar
     Rectangle {
         width: 400; height: 40
         color: "#4a9ea1"
         y: view.currentItem.y;
         Behavior on y { SpringAnimation { spring: 4; damping: 0.8 } }
    }
  }
}
