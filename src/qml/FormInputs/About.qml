import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

RowLayout {
  property alias text: textField.text

  IText {
    text: qsTr("About")
  }

  ITextField {
    id: textField
    validator: RegExpValidator {
      regExp: /^([\w]{1,64})$/
    }
  }
}
