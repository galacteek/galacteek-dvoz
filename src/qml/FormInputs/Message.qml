import QtQuick 2.2
import QtQuick.Dialogs 1.1

MessageDialog {
    property alias messageTitle: message.title
    property alias messageText: message.text

    id: message
    visible: false
    title: "Message"
}
