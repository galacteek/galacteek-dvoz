import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

RowLayout {
  property alias text: textField.text
  property alias length: textField.length
  property alias field: textField

  signal edited

  IText {
    text: qsTr("Headline")
  }

  ITextField {
    id: textField
    validator: RegExpValidator {
      regExp: /^([\w\s!?=]{1,128})$/
    }

  }
}
