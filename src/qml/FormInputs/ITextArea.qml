import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.1

TextArea {
  selectByMouse: true

  style: TextAreaStyle {
    textColor: "white"
    selectionColor: "steelblue"
    selectedTextColor: "#eee"
    backgroundColor: "#323232"
  }
}
