import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Controls.Styles 1.1

ComboBox {
  font.pixelSize: 20

  background: Rectangle {
    implicitWidth: 300
    implicitHeight: 42
    opacity: enabled ? 1 : 0.3
    color: control.down ? "#d0d0d0" : "#e0e0e0"
  }
}
