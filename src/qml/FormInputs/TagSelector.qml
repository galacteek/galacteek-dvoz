import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import "../models/sparql"

RowLayout {
  property TagsModel tModel: TagsModel {
    id: tagsModel
    onSparkle: {
      console.debug('OK')
    }
  }

  property string label: qsTr("Tag")
  property alias tag: combo.currentText

  function ld() {
    return combo.ldTr()
  }

  IText {
    text: label
  }

  IComboBox {
    id: combo
    textRole: "tag"
    model: tagsModel
    font.pixelSize: 20

    function ldTr() {
      let item = model.get(currentIndex)

      return {
        "@id": "i:/Language/" + item.name,
        "@type": "Language",
        "name": item.name,
        "alternateName": item.code
      }
    }
  }
}
