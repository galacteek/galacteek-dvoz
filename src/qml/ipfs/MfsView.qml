import QtQuick 2.2
import QtQuick.Controls 1.4

TreeView {
  id: mfsView
  anchors.fill: parent
  model: modelMfs

  signal ipfsObjectSelected(string path, string url, string mimeType)

  function expandRecursive(idx) {
    for (var i=0; i < modelMfs.rowCount(idx); i++) {
      var index = modelMfs.index(i, 0, idx)

      if(!mfsView.isExpanded(index)) {
        mfsView.expand(index)
      }
      var rowCount = modelMfs.rowCount(index)

      if (rowCount > 0) {
        expandRecursive(index)
      }
    }
    mfsView.expand(idx)
  }

  function expandAll() {
    for(var i=0; i < modelMfs.rowCount(); i++) {
      var index = modelMfs.index(i, 0)
      expandRecursive(index)
    }
  }

  Component.onCompleted: {
    expandAll()
  }

  onDoubleClicked: {
    // Get the path, URL and MIME

    var path = modelMfs.data(index, modelMfs.ipfsPathRoleValue())
    var url  = modelMfs.data(index, modelMfs.ipfsUrlRoleValue())
    var mime = modelMfs.data(index, modelMfs.mimeTypeRoleValue())

    console.debug(path)
    console.debug(url)
    console.debug(mime)

    if (path && url && mime) {
      mfsView.ipfsObjectSelected(path, url, mime)
    }
  }

  TableViewColumn {
    id: nameColumn
    role: "name"
    title: qsTr("Name")
    width: mfsView.width / 2
  }

  TableViewColumn {
    role: "mimeType"
    title: qsTr("MIME")
  }

  itemDelegate: Item {
    Text {
      color: styleData.textColor
      elide: styleData.elideMode
      text: styleData.value
    }
  }
}
